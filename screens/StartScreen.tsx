import { useState } from 'react';
import { Pressable, StyleSheet, TextInput } from 'react-native';
import { TouchableWithoutFeedback, Keyboard, Button } from 'react-native';
import EditScreenInfo from '../components/EditScreenInfo';
import { Text, View } from '../components/Themed';
import { RootTabScreenProps } from '../types';

export interface Location {
  lat: number,
  lon: number,
  delta: number,
  name: string
}
export interface Game {
  name: string,
  start: Location,
  locs: [Location]
}
export default function StartScreen({ navigation }: RootTabScreenProps<'TabOne'>) {
  const [text, setText] = useState('');

  function checkCode() {
    let game = null;
    console.log(text);
    if (text == "111111") {
      game = {
        name: "ScubRun TEST",
        start: {
          name: "TestStart",
          lat: 38.994711145825754,
          lon: -76.94062442973559,
          delta: 150
        },
        locs: [
        {
          name: "Test",
          lat: 38.9954505, 
          lon: -76.9412938,
          delta: 20
        },
        {
          name: "Test2",
          lat: 38.9954505, 
          lon: -76.9412938,
          delta: 50
        },
      ]
    };
    }
    else if (text == "123456") {
      game = {
        name: "ScubRun",
        start: {
          name: "Testudo",
          lat: 38.98598115181125, 
          lon: -76.94457883468553,
          delta: 50
        },
        locs: [
          {
          name: "Scub I",
          lat: 38.982080,
          lon: -76.940473,
          delta: 15
        },
        {
          name: "Scub II",
          lat: 38.98356200693396, 
          lon: -76.9446559764491,
          delta: 30
        },
        {
          name: "Scub III",
          lat: 38.98906919911225,
          lon: -76.94593421894848,
          delta: 30
        },
        {
          name: "Scub IV",
          lat: 38.98990665499968, 
          lon: -76.93838561894613,
          delta: 25
        },
        {
          name: "Scub V",
          lat: 38.98555812761069, 
          lon: -76.94831199612257,
          delta: 20
        },
      ]
    };
    } 
    if (game == null) {
      console.error("Unknown code.");
    } else {
      navigation.navigate('Game', {game: game});
    }
  }

  return (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
    <View style={styles.container}>
      <Text style={styles.title} >Welcome to ScubRun!</Text>
      <View style={styles.separator} lightColor="#eee" darkColor="rgba(255,255,255,0.1)" />
      <Text>Enter "123456" for ScubRun any%</Text>
      <View style={{    marginVertical: 10}} lightColor="#eee" darkColor="rgba(255,255,255,0.1)" />
      <View
      style={{
        backgroundColor: "#FFFFFF",
        borderBottomColor: '#000000',
        borderBottomWidth: 1,
        padding: 5,
        width: "30%"
      }}>
        
      <TextInput
        numberOfLines={1}
        style={{padding: 1,
          fontSize:30
        }}
        onChangeText={newText => setText(newText)}
        keyboardType='number-pad'
        maxLength={6}
        textAlign={'center'}

      /></View>
      <View style={{marginVertical: 20}} lightColor="#eee" darkColor="rgba(255,255,255,0.1)" />
      <Pressable
    style={styles.button}
    color="#FFFFFF"
    onPressOut={checkCode}
  ><Text
  style={styles.text}>Start Game</Text></Pressable>
    </View>
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 30,
    fontWeight: 'bold',
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 12,
    paddingHorizontal: 32,
    borderRadius: 4,
    borderWidth: 1,
    borderColor: '#FFFFFF',
    elevation: 3,
    backgroundColor: 'black',
  },
  text: {
    fontSize: 16,
    lineHeight: 21,
    fontWeight: 'bold',
    letterSpacing: 0.25,
    color: 'white',
  },
});

