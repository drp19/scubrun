import React, { useEffect, useState } from 'react';
import { Dimensions, Modal, Pressable, Share, StyleSheet } from 'react-native';
import BouncyCheckbox from "react-native-bouncy-checkbox";
import EditScreenInfo from '../components/EditScreenInfo';
import { Text, View } from '../components/Themed';
import { RootTabScreenProps } from '../types';
import { Game, Location } from './StartScreen';
import * as GeoLocation from 'expo-location';
import { Stopwatch, Timer } from 'react-native-stopwatch-timer'
import MapView, { Circle } from 'react-native-maps';
const emojiUnicode = require("emoji-unicode");
import Moment from 'moment';


interface IState {
  location: GeoLocation.LocationObject | null;
  found: Array<boolean>;
  atStart: boolean;
  stopwatchStart: boolean;
  stopwatchReset: boolean;
  done: boolean;
  mapVisible: boolean;
  markerLoc: [number, number, number, string];
}
class GameScreen extends React.Component<any, IState> {
  constructor(props) {
    super(props);
    console.log( this.props.route.params.game.locs.map((x) => false));
    this.state = {
      location: null,
      found: this.props.route.params.game.locs.map((x) => false),
      stopwatchStart: false,
      stopwatchReset: false,
      done: false,
      mapVisible: false,
      markerLoc: [this.props.route.params.game.start.lat, this.props.route.params.game.start.lon, 
        this.props.route.params.game.start.delta, this.props.route.params.game.start.name]
    }
    this.onScreenFocus = this.onScreenFocus.bind(this);
    this.reset = this.reset.bind(this);
    this.start = this.start.bind(this);
    this.resetFound = this.resetFound.bind(this);
    this.share = this.share.bind(this);
    this.canStart = this.canStart.bind(this);
    this.renderModal = this.renderModal.bind(this);
  }

  resetFound() {
    this.setState({found: this.props.route.params.game.locs.map((x) => false),
    markerLoc: [this.props.route.params.game.start.lat, this.props.route.params.game.start.lon, 
      this.props.route.params.game.start.delta, this.props.route.params.game.start.name],
      stopwatchStart: false, stopwatchReset: true, done: false});
  }

  share() {
    if (this.state.time == "00:00:00:000" || this.state.stopwatchStart) {
      return;
    }
    console.log(this.state.found);

    let message = this.props.route.params.game.name + "\n\n" + this.state.found.reduce((a,f) => 
      f ? a+'🟩' : a+'⬛', "") + "\n" + this.state.time;
    console.log(message);
    Share.share({
        message: 
        message
    });
  }

  renderModal() {
    const modalVisible = this.state.mapVisible;
    console.log(this.state.markerLoc);
    
    return (
        <Modal
          animationType="slide"
          transparent={true}
          visible={modalVisible}
          onRequestClose={() => {
            //Alert.alert("Modal has been closed.");
            this.setState({mapVisible: false});
        }}>
          <View style={styles.centeredView}>
            <View style={styles.modalView}>
            <MapView
            style={{
              width: Dimensions.get('window').width,
              height: Dimensions.get('window').height*.75,
            }}
            
            showsUserLocation={true}
            initialRegion={{
              latitude: this.state.markerLoc[0],
              longitude: this.state.markerLoc[1],
              latitudeDelta: 0.0052,
              longitudeDelta: 0.0051,
            }}>
              <Circle center={{latitude: this.state.markerLoc[0], longitude: this.state.markerLoc[1]}} radius={this.state.markerLoc[2]}
            /></MapView>
          <Pressable
            style={[styles.button, styles.buttonClose]}
            onPress={() => this.setState({mapVisible: false})}>
            <Text style={styles.textStyle}>Hide Map</Text>
          </Pressable>
            </View>
          </View>
        </Modal>
    );
  }
  
  onScreenFocus() {
      console.log("Focus");
      this.resetFound();
  }

  distance(lat1: number, lon1: number, lat2: number, lon2: number) {
    var p = 0.017453292519943295;    // Math.PI / 180
    var c = Math.cos;
    var a = 0.5 - c((lat2 - lat1) * p)/2 + 
            c(lat1 * p) * c(lat2 * p) * 
            (1 - c((lon2 - lon1) * p))/2;

    return 12742 * Math.asin(Math.sqrt(a)); // 2 * R; R = 6371 km
  }

  reset() {
    this.setState({ stopwatchStart: false, stopwatchReset: true, done: false});
    this.resetFound();
  }

  start() {
    if (this.canStart()) {
      this.setState({stopwatchStart: !this.state.stopwatchStart, stopwatchReset: false});
    }
  }

  canStart() {
    return ((this.state.stopwatchReset && this.state.atStart) || (this.state.stopwatchStart || !this.state.stopwatchReset));
  }

  render() {
    if (!this.state.location) {
      this.state.location = {"coords":{"altitude":33.781028747558594,"altitudeAccuracy":13.79312515258789,"latitude":38.995426908098935,"accuracy":45,"longitude":-76.94168280831997,"heading":-1,"speed":-1},"timestamp":1649538242151.649};
    }
    if (!this.props.route.params.game) {
      return;
    }
    const game: Game = this.props.route.params.game;
    let isCloseStart = (this.distance(this.state.location.coords.latitude, this.state.location.coords.longitude, this.state.markerLoc[0], this.state.markerLoc[1])*1000 < this.state.markerLoc[2]);
    this.state.atStart = isCloseStart;
    let startColor = isCloseStart ? 'rgba(00, 255, 00, .6)' : '#000000';
    let startButtonColor = this.canStart() ?  '#000000': '#303030';
    let shareButtonColor = (!(this.state.time == "00:00:00:000" || this.state.stopwatchStart)) ? '#000000' : '#303030';
    let accuracy = this.state.location.coords.accuracy;
    accuracy = accuracy ? Math.trunc(accuracy) : "?";
    let start = (<View 
      key={"start"}
      style={{
      width: "90%",
      height: "10%",
      borderWidth: 2,
      borderColor: "#FFFFFF",
      backgroundColor: startColor,
      alignItems: 'center',
      justifyContent: 'left',
      flexDirection: 'row',
    }}
    ><Text style={{
      paddingLeft:10,
      fontWeight:"bold"
          }}>{this.state.markerLoc[3]}</Text>
      <View style={{
        backgroundColor: 'rgba(255,255, 255,0.0)',
        paddingLeft: 95
      }}>
    <Pressable
      style={[styles.button, styles.buttonOpen]}
      onPress={() => this.setState({mapVisible: true})}
    >

      <Text style={styles.textStyle}>Show Location</Text>
    </Pressable></View></View>);

    
    let boxes = game.locs.map((l, idx) => {
      let color = this.state.found[idx] ? 'rgba(00, 255, 00, .6)' : "#000000";
     
      return (<View 
        key={"box"+idx}
        onTouchStart={() => {
          if (!this.state.stopwatchStart) {
            return;
          }
          let dist = this.distance(this.state.location.coords.latitude, this.state.location.coords.longitude, l.lat, l.lon)*1000;
          if (dist < l.delta) {
            let newFound = this.state.found;
            newFound[idx] = true;
            this.setState({found: newFound});
            if (newFound.every((element => element === true))) {
              this.setState({stopwatchStart: false, done:true});
            }
          } else {
            console.log("Not close enough");
          }
       }}
      style={{
        width: "90%",
        height: "10%",
        borderWidth: 2,
        borderColor: "#FFFFFF",
        backgroundColor: color,
        alignItems: 'center',
        justifyContent: 'left',
        flexDirection: 'row',
      }}
      ><Text style={{
        paddingLeft:10,
        fontWeight:"bold"
            }}>{l.name}</Text>
      </View>);
    });
    return (
      <View style={styles.container}>
        {this.renderModal()}
        <Text style={styles.title}>{this.props.route.params.game.name}</Text>
        <View style={styles.separator} lightColor="#eee" darkColor="rgba(255,255,255,0.1)" />
        <View style={{
          alignContent: 'left'
        }}><Text style={{
          fontSize: 18
        }}>Start at:</Text></View>
        {start}
        <Text style={{paddingVertical: 5}}>Current GPS accuracy: {accuracy}m</Text>
        {boxes}
        <View style={{flexDirection: "row"}}><View style={{marginHorizontal: 19,
    height: 1,}} lightColor="#eee" darkColor="rgba(255,255,255,0)" /><Stopwatch  msecs start={this.state.stopwatchStart}
          reset={this.state.stopwatchReset}
          getTime={(time) => this.state.time = time} /></View>
        <View style={{
        alignItems: 'center',
        justifyContent: 'left',
        flexDirection: 'row',
        }}>
          <Pressable
  
          style={[styles.finishButton, {backgroundColor: startButtonColor}] }
          color={startButtonColor}
          onTouchStart={this.start}
          ><Text
            style={styles.text}>{this.state.stopwatchStart ? "Pause": "Start"}</Text></Pressable>
            <Pressable
          style={styles.finishButton}
          onTouchStart={this.reset}
          ><Text
            style={styles.text}>Reset</Text></Pressable>
          </View>
          {/* <Text>{text}</Text> */}
          <Pressable
          style={[styles.finishButton, {backgroundColor: shareButtonColor}] }
          color="#FFFFFF"
          onTouchStart={this.share}
          ><Text>Share</Text></Pressable>
        </View>

    );
  }

  componentDidMount() {
    this.props.navigation.addListener('focus', this.onScreenFocus);
    (async () => {
      let { status } = await GeoLocation.requestForegroundPermissionsAsync();
      if (status !== 'granted') {
        setErrorMsg('Permission to access location was denied');
        return;
      }
      while (true) {
        let location = await GeoLocation.getCurrentPositionAsync({accuracy:5});
        if (location != null) {
          this.setState({location: location});
        }
      }
    })();
  }
  
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 30,
    fontWeight: 'bold',
  },
  separator: {
    marginVertical: 15,
    height: 1,
    width: '80%',
  },
  button: {
    alignItems: 'center',
    elevation: 2,
    justifyContent: 'center',
    paddingVertical: 12,
    paddingHorizontal: 32,
    borderRadius: 4,
    borderWidth: 1,
    borderColor: '#FFFFFF',
    elevation: 3,
    backgroundColor: 'black',
  },
  finishButton: {
    alignItems: 'center',
    elevation: 2,
    justifyContent: 'center',
    paddingVertical: 12,
    marginHorizontal: 5,
    marginBottom: 10,
    paddingHorizontal: 32,
    borderRadius: 4,
    borderWidth: 1,
    borderColor: '#FFFFFF',
    elevation: 3,
  },
  text: {
    fontSize: 16,
    lineHeight: 21,
    fontWeight: 'bold',
    letterSpacing: 0.25,
    color: 'white',
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5
  },
  buttonOpen: {
    fontWeight: 'bold',
    backgroundColor: "#FFFFFF",

  },
  buttonClose: {
    backgroundColor: "#2196F3",
  },
  textStyle: {
    color: "black",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center"
  }
});

export default GameScreen;